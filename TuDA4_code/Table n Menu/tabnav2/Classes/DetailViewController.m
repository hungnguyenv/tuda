//
//  DetailViewController.m
//
//  Created by Keith Harrison on 12/07/2010 http://useyourloaf.com
//  Copyright (c) 2010 Keith Harrison. All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//  Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//
//  Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//
//  Neither the name of Keith Harrison nor the names of its contributors
//  may be used to endorse or promote products derived from this software
//  without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER ''AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

#import "DetailViewController.h"

@implementation DetailViewController

@synthesize textLabel, remindButton, item;

#pragma mark -
#pragma mark === Initialization and shutdown ===
#pragma mark -

- (void)viewDidLoad {
	
	textLabel.text = [NSString stringWithFormat:@"Item %d", item];

	NSUInteger interval = (item + 1) * 5;
	NSString *buttonTitle = [NSString stringWithFormat:@"%d seconds", interval];
	[remindButton setTitle:buttonTitle forState:UIControlStateNormal];
}

- (void)viewDidUnload {
    [super viewDidUnload];
	self.textLabel = nil;
	self.remindButton = nil;
}

- (void)dealloc {
	[remindButton release];
	[textLabel release];
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return YES;
}

#pragma mark -
#pragma mark === View Actions ===
#pragma mark -

- (void)scheduleNotification {
	
	[[UIApplication sharedApplication] cancelAllLocalNotifications];
	
	Class cls = NSClassFromString(@"UILocalNotification");
	if (cls != nil) {
		
		NSUInteger interval = (item + 1) * 5;
		NSString *alertText = [NSString stringWithFormat:@"%d Second Reminder",interval];
		
		UILocalNotification *notif = [[cls alloc] init];
		notif.fireDate = [NSDate dateWithTimeIntervalSinceNow:interval];
		notif.alertBody = alertText;
		notif.soundName = UILocalNotificationDefaultSoundName;
		
		[[UIApplication sharedApplication] scheduleLocalNotification:notif];
		[notif release];
		
		remindButton.enabled = NO; 
	}
}

@end