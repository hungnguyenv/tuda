//
//  TableViewCell.swift
//  TableViewLazyLoadingDemo
//
//  Created by Shinkangsan on 12/16/16.
//  Copyright © 2016 Sheldon. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    //init indicator - MY Code
    var activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        //indicator - MY Code
        self.activityIndicator.center = imgView.center
        self.activityIndicator.hidesWhenStopped = true
        self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        self.addSubview(activityIndicator)
        
        self.activityIndicator.startAnimating()
        //indicator
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
