//
//  ViewController.swift
//  ProgressView
//
//  Created by Vasil Nunev on 23/07/2017.
//  Copyright © 2017 nunev. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var imgView: CustomImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        imgView.setImage(from: "https://vignette.wikia.nocookie.net/badcreepypasta/images/e/e4/6594.jpg/revision/latest?cb=20170605035518.jpg")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

