//
//  ViewController.swift
//  CustomNavBar
//
//  Created by Mark Moeykens on 7/21/17.
//  Copyright © 2017 Mark Moeykens. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var stepForwardButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        setCustomTitle()
        setCustomBackImage()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 2, delay: 4, options: [.allowUserInteraction, .autoreverse, .repeat, .curveEaseIn], animations: {
            self.stepForwardButton.alpha = 0.4
        })
    }
    
    func setCustomBackImage() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationController?.navigationBar.backIndicatorImage = #imageLiteral(resourceName: "StepBack")
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "StepBack")
    }
    
    func setCustomTitle() {
        let label1 = UILabel()
        label1.text = "Fantastic"
        label1.font = UIFont(name: "Menlo", size: 30)
        label1.textColor = .white
        label1.sizeToFit()
        
        let label2 = UILabel()
        label2.text = "Journey"
        label2.font = UIFont(name: "Redressed", size: 30)
        label2.textColor = .white
        label2.sizeToFit()
        
        let stackView = UIStackView(arrangedSubviews: [label1, label2])
        stackView.axis = .horizontal
        stackView.frame.size.width = label1.frame.width + label2.frame.width
        stackView.frame.size.height = max(label1.frame.height, label2.frame.height)
        
//        navigationItem.titleView = stackView
    }
}
