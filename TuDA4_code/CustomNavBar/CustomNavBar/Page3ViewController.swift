//
//  Page3ViewController.swift
//  CustomNavBar
//
//  Created by Mark Moeykens on 7/23/17.
//  Copyright © 2017 Mark Moeykens. All rights reserved.
//

import UIKit

class Page3ViewController: UIViewController {
    
    @IBOutlet weak var stepForwardButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 2, delay: 4, options: [.allowUserInteraction, .autoreverse, .repeat, .curveEaseIn], animations: {
            self.stepForwardButton.alpha = 0.4
        })
    }
}
